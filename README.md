# TrinityCore OpenShift templates

## What's in here

This template tries to simplify the deployment of a TrinityCore server.

It uses debian:stretch as its base, installs building tools in a new Image, then uses S2I to build the actual server binaries.

It also uses the extraction binaries to convert the game datafiles into Trinity data files needed for running the server.

You will need 3 PVs:
* One for the DB PVC, it will keep the mariadb datafiles [trinitycore-dbdata.pv.yaml](trinitycore-dbdata.pv.yaml)
* One for the game datafiles, likely an NFS share where you'll need to copy the `Data` game directory contents. [gamedata.pv.yaml](gamedata.pv.yaml)
* One for the extracted server data files. [trinitycore-datafiles.pv.yaml](trinitycore-datafiles.pv.yaml)


## How to use it

Import the [trinitycore-template.yaml](trinitycore-template.yaml) into an OpenShift Project like this

```sh
oc create -f trinitycore-template.yaml
```

Then, from the Web Interface, choose Add to Project -> Select from Project -> TrinityCore on OpenShift.

The Images start building. When they are all built, a Job will start extracting datafiles.

When the Jobs is done, you just scale up the DB and the trinitycore-server DeploymentConfig to 1.

## How it works

The whole container building process is as follows:

* The trinitycore-sti image, containing the gcc toolchain and devel libs, is built based on debian:latest using a Dockerfile
* The trinitycore-runtimebase, containing just the runtime libs, is built based on debian:latest using a Dockerfile
* The trinitycore-build image, containing the compiled TrinityCore source code, is built based on the trinitycore-sti image
* The trinitycore-server image, containing just the minimal TrinityCore server binaries, is built based on the combination of trinitycore-runtimebase as its base, with files injected from the trinitycore-build image.

